
\chapter{Gestión económica}

Una vez realizada la planificación temporal existen datos suficientes como para desarrollar el presupuesto del proyecto. Éste justifica la viabilidad económica de cara a la empresa \textit{Maths for More S.L.} .

\section{Identificación y estimación de costes}

La identificación de costes es el paso previo a la elaboración del presupuesto. Esta sección analiza desde el punto de vista económico los diferentes tipos de recursos del proyecto, definidos previamente durante la planificación.

\subsection{Costes directos}

\paragraph{Recursos humanos.}

Este proyecto está realizado íntegramente por el estudiante del TFG, asumiendo cada uno de los roles correspondientes durante la realización de cada tarea.

\medskip

El salario bruto de cada rol ha sido extraído de la \textit{Guía del mercado laboral 2019}\cite{GuiaHays2019} de la empresa \textit{Hays}, una de las empresas de recursos humanos más grandes del mundo, y webs reconocidas como \textit{PayScale}\cite{TechnicalWriterSalaryBarcelona} e \textit{Indeed}\cite{TraductorSalaryBarcelona}. Los precios se han calculado en base a un trabajo anual de 2008h y un nivel de experiencia equivalente a \enquote*{junior}, es decir, entre 0 y 2 años de experiencia.

\medskip

Una vez extraído el salario bruto de cada empleado se ha realizado una aproximación del costo que supone para la empresa tener a dicho trabajador, ya que existen más costes como el de la Seguridad Social o formación. La fórmula utilizada es la siguiente:

\begin{equation}
	\begin{split}
		CosteEmpleadoEmpresa=SueldoBrutoEmpleado\cdot1,35
	\end{split}
\end{equation}

\medskip

De esta investigación, se ha obtenido la siguiente tabla con los costes por tarea, fase y rol.

\vspace{5mm}

\begin{minipage}{\linewidth}
	\captionof{table}{Coste de RRHH por rol, fases y tareas}
	\begin{adjustbox}{center,nofloat=figure}
		
		% This is a non-float method. Look more here https://tex.stackexchange.com/questions/8625/force-figure-placement-in-text.
		\includegraphics[width=\textwidth,keepaspectratio]{Tabla_Coste_RRHH.png}
	\end{adjustbox}
\end{minipage}

\paragraph{Recursos hardware y software.} Por otro lado, también existe un gasto en los recursos hardware y software desde el inicio del proyecto.

\medskip

Las amortizaciones se han calculado en base a una vida útil que en ningún caso supera a los años máximos que propone la Agencia Tributaria\cite{AgenciaTributariaAmortizaciones}. Se ha extraído el precio amortizado por hora y se ha calculado el importe del gasto del proyecto teniendo en cuenta que éste utiliza dichos recursos durante 540h.

\medskip

Los costes se pueden observar en la siguiente tabla. No han sido incluidos recursos gratuitos.

\vspace{5mm}

\begin{minipage}{\linewidth}
	\captionof{table}{Coste de recursos materiales y software}
	\begin{adjustbox}{center,nofloat=figure}
		
		% This is a non-float method. Look more here https://tex.stackexchange.com/questions/8625/force-figure-placement-in-text.
		\includegraphics[width=0.6\textwidth,keepaspectratio]{Tabla_Coste_Recursos.png}
	\end{adjustbox}
\end{minipage}

\vspace{5mm}

\subsection{Costes indirectos}

Durante el desarrollo existen costes indirectos como consecuencia de las actividades que se realizan. Dado que el TFG se realiza en una empresa, es el coste de recursos generales (agua, oficina, Internet... etc).

\medskip

Las amortizaciones se han calculado de la misma forma que en el apartado anterior. Sin embargo, en la columna \enquote*{Vida útil} hay algunas filas sin datos debido a que son consumibles y han sido calculados de diferentes formas.

\medskip

Los servicios, la conexión a Internet y el local (200m2 en el centro de Barcelona) han sido calculados a partir de las facturas mensuales de gasto de la empresa y estimando que el proyecto cuesta 4 meses de alquiler, ya que no existe la posibilidad de pagar en unidades menores al mes. El resultado se divide en partes iguales entre los 32 trabajadores de la oficina. En cambio, los snacks han sido calculados en base al gasto individual del estudiante de 6\euro\ cada 40h de trabajo.

\medskip

Los costes indirectos por lo tanto son los siguientes:

\vspace{5mm}

\begin{minipage}{\linewidth}
	\captionof{table}{Costes indirectos}
	\begin{adjustbox}{center,nofloat=figure}
		% This is a non-float method. Look more here https://tex.stackexchange.com/questions/8625/force-figure-placement-in-text.
		\includegraphics[width=0.6\textwidth,keepaspectratio]{Tabla_Coste_Indirecto.png}
	\end{adjustbox}
\end{minipage}

\vspace{5mm}

\subsection{Contingencias}

Como todo proyecto, existe un riesgo de necesitar un presupuesto mayor al elaborado debido a los sucesos inesperados. Por ello existe una partida destinada a contener esas posibles desviaciones. Para este proyecto se utilizará un fondo de contingencias del 10\%, una cifra encontrada de manera empírica y que en asignaturas como GPS se nos ha impulsado a utilizar por su buen resultado.

\subsection{Coste de incidentes}

Finalmente se presenta el coste de los incidentes que pueden surgir en el proyecto.

\medskip

Para ello, se tiene en cuenta el caso pesimista de los problemas presentados en el plan de acción de la planificación, donde se habla del incremento de horas de trabajo. Este aumento de horas probablemente se daría a la mitad del proyecto, durante la fase de \textit{Desarrollo de MathType for iOS}. Debido a eso se ha calculado un coste asumiendo que el aumento de horas empieza en la tarea \enquote*{Cambio en el tipo de imagen} hasta la finalización del proyecto.

\medskip

Por otra parte, se ha tenido en cuenta la pérdida del Mac Mini, la herramienta de desarrollo principal. Cuyo coste, gracias a que la entrega podría ser el mismo día y que el estudiante tiene horario flexible, se limitaría a un coste material.

\vspace{5mm}

\begin{minipage}{\linewidth}
	\captionof{table}{Costes de incidentes}
	\begin{adjustbox}{center,nofloat=figure}
		
		% This is a non-float method. Look more here https://tex.stackexchange.com/questions/8625/force-figure-placement-in-text.
		\includegraphics[width=0.6\textwidth,keepaspectratio]{Tabla_Coste_Incidentes.png}
	\end{adjustbox}
\end{minipage}

\vspace{5mm}

\section{Coste total}

De las secciones anteriores, se puede extraer la siguiente tabla con el presupuesto del proyecto.

\vspace{5mm}

\begin{minipage}{\linewidth}
	\captionof{table}{Coste total}
	\begin{adjustbox}{center,nofloat=figure}
		
		% This is a non-float method. Look more here https://tex.stackexchange.com/questions/8625/force-figure-placement-in-text.
		\includegraphics[width=0.4\textwidth,keepaspectratio]{Tabla_Coste_Total.png}
	\end{adjustbox}
\end{minipage}

\vspace{5mm}

\section{Control de gestión}

Para el control económico del proyecto se hace uso de indicadores de control. Concretamente de cumplimiento y eficiencia.

\medskip

El primer indicador es el de finalización de tareas. Dicho indicador tiene que ver con la conclusión de las tareas del proyecto llevando una cuenta de las ya completadas. De esta manera, es posible medir el nivel de realización del TFG.

\medskip

El segundo indicador permite medir el nivel de ejecución de las tareas según los recursos utilizados. Al finalizar cada tarea se anotan las horas y recursos invertidos reales y, dichos datos, se comparan con las estimaciones realizadas haciendo uso de las siguientes fórmulas presentadas durante la asignatura de GEP\cite{ApuntesAsignaturaGEP}.

\begin{equation}
	\begin{split}
		Desv\acute{i}oEnCoste &= (CE - CR) \cdot CHR \\
		Desv\acute{i}oEnConsumo &= (CHE - CHR) \cdot CE \\
	\end{split}
\end{equation}

\medskip

Donde: CE=Coste Estimado; CR=Coste Real; CHR=Consumo de Horas Real; CHE=Consumo de Horas Estimado.


\medskip

Todo ello permite saber el estado del proyecto respecto al consumo de recursos y también conocer las posibles desviaciones, para así calcular si el proyecto puede soportarlas haciendo uso de la partida de contingencias.
