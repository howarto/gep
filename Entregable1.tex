\documentclass[11pt,a4paper]{report}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage[hidelinks=true]{hyperref}
\usepackage[spanish,es-tabla]{babel}
\usepackage[backend=biber]{biblatex}
\usepackage{csquotes}
\usepackage{glossaries}
\usepackage{adjustbox}
\usepackage{float}
\usepackage{enumitem}
\usepackage[toc,page]{appendix}


\author{
	Diego Lao Tebar \\
	Ponente: Cristina Gómez Seoane \\
	Director: Juan Lao Tebar \\
	Profesor de GEP: Joan Carles Gil Martin \\
	Especialidad: Ingeniería del Software
}
\title{
	{Integración de un editor de expresiones matemáticas en un entorno educativo basado en dispositivos móviles} \\
	{\large GEP - Primer entregable} \\
	{\large UPC - FIB} \\
	\vspace{1cm}
	{\includegraphics[width=0.5\columnwidth]{upc.png}}
}

\addbibresource{references.bib}
\graphicspath{{images/}}
% Propiedades del estilo del documento
\setlength\parindent{0pt}

\begin{document}
	\maketitle
	
	\tableofcontents
	
	\chapter{Contexto}
	
	\section{Introducción}
	
	Este proyecto es un \textit{Trabajo de Final de Grado (TFG)} de la especialidad de Ingeniería del Software de la Facultat d'Informàtica de Barcelona (Universitat Politècnica de Barcelona) en modalidad B. Concretamente se trabajará en la empresa Maths for More S.L. .
	
	\medskip
	
	\textit{Maths for More S.L.} es una empresa dentro del sector de las matemáticas y la educación. Desarrollan productos bajo el nombre de \textit{WIRIS} y \textit{MathType} para grandes plataformas \textit{LMS (Learning Management System)} como \textit{Moodle} y distintos editores web.\cite{MathTypeWebReference}
	
	\medskip
	
	Actualmente, uno de sus productos principales es \textit{MathType}, un editor de fórmulas matemáticas dirigido a profesores, alumnos y editores generales de documentos matemáticos que permite la edición de fórmulas tanto usando una barra de herramientas como de forma manuscrita (\textit{handwriting}).
	
	\medskip
	
	\textit{MathType} destaca por su integración en plataformas de terceros muy populares como \textit{Google Docs}\cite{GoogleDocsWebsite}, \textit{Moodle}\cite{MoodleWebsite} o \textit{Microsoft Office}\cite{MicrosoftOfficeWebsite} y también por la facilidad de integración en nuevas plataformas. Debido a esto, se ha decidido empezar su expansión a otros sistemas operativos de escritorio y móvil. Este TFG pretende ser la expansión de \textit{MathType} a iOS y lo hará bajo el nombre de \textit{MathType for iOS}.
	
	\section{Actores implicados}
	
	Este proyecto tiene distintos actores que se ven afectados por su desarrollo. En las siguientes secciones se detallarán y se explicarán.
	
	\subsection{Maths for More S.L.}
	
	Empresa donde se realiza el proyecto. El proyecto afecta a toda la empresa debido a que es una inversión por parte de ésta para expandir su producto en el mercado. Por ello, también se ven afectados todos los departamentos como el comercial, el de soporte u otros departamentos técnicos de la empresa.
	
	\subsection{Equipo de desarrollo y evaluador del TFG}
	
	Es el equipo que tendrá en cuenta que el proyecto será evaluado como TFG, aportarán diferentes puntos de vista y lo desarrollarán.
	
	\medskip
	
	Por un lado estará Diego Lao Tebar, el estudiante responsable que trabaja en \textit{Maths for More S.L.} y que realizará el proyecto tomando las decisiones para terminarlo de la mejor forma posible. Y por otro lado están: Juan Lao Tebar, el director del proyecto; y Cristina Gómez Seoane, la ponente del proyecto. Su cometido es el de guiar y supervisar el TFG.
	
	\subsection{Los usuarios}
	
	Los usuarios a los que va destinado \textit{MathType} son principalmente alumnos, profesores y editores que trabajan con fórmulas matemáticas y necesitan una herramienta para editar y visualizar dichas fórmulas matemáticas. Este proyecto les ofrece esas funcionalidades desde dispositivos iOS.
	
	\subsection{Los usuarios clientes de Maths for More S.L.}
	
	Ya hay usuarios que son clientes que utilizan el editor de fórmulas matemáticas \textit{MathType}. Este proyecto complementará la versión de escritorio o web y les permitirá el uso de \textit{MathType} en el sistema operativo iOS, pudiendo realizar el trabajo que hacen desde dispositivos móviles iOS.
	
	\subsection{Usuarios \enquote*{validadores}}
	
	Los usuarios \enquote*{validadores} son aquellos usuarios que se escogen para realizarles pruebas de validación del producto. Reportan posibles problemas del producto para así poder revisar cambios que ayuden a mejorar.
	
	\subsection{Otros stakeholders}
	
	Existen otros actores implicados como por ejemplo programadores u otros sistemas de la competencia iguales o parecidos. El código de las integraciones de \textit{MathType} es de código abierto, por lo que puede ser revisado por éstos actores con el fin de aprender o copiar el código.
	
	\begingroup
	\renewcommand{\cleardoublepage}{}
	\renewcommand{\clearpage}{}
	\chapter{Estado del arte}
	\endgroup
	
	Dentro del mundo de la edición de fórmulas matemáticas, existen diferentes estándares para la representación de fórmulas. Los principales son \textit{MathML}\cite{MathML} y \LaTeX\cite{LatexBook}. Estos estándares han provocado que los sistemas de edición de fórmulas sean heterogéneos y que no puedan comunicarse entre sí de forma sencilla, ya que cada uno implementa su propio estándar o elige uno ya existente de forma arbitraria. Sin embargo, han facilitado que dos editores que hayan implementado el mismo estándar puedan comunicarse entre sí.
	
	\medskip
	
	Este problema de heterogeneidad ha provocado que el exportar fórmulas sea también complicado debido a que hay que responder a la pregunta de \enquote*{¿En qué formato exportar?}. La solución por parte de algunos de los editores ha sido no dejar exportar, exportar a diferentes estándares o simplemente exportar en un formato genérico aceptado por casi todos, una imagen.
	
	\medskip
	
	Insertar en un editor de texto una imagen es algo trivial hoy en día. Por lo que es una de las soluciones que mejor funcionan en aquellos editores de fórmulas que buscan exportar sus fórmulas a otros programas, con la seguridad de que podrán ser utilizadas.
	
	\medskip
	
	A parte del problema de heterogeneidad comentado, ha existido otro problema crítico, la creación de un sistema simple para editar las fórmulas. Los estándares de representación de fórmulas matemáticas son muy extensos y esto se traduce en que una interfaz de usuario no puede enseñar tanta información a la vez. Por ello, muchos de los editores existentes en el mercado tienen interfaces poco usables y además muy diferentes entre sí.
	
	\medskip
	
	Actualmente existe un programa software llamado \textit{MathType} que intenta solucionar estos problemas mediante el uso del estándar \textit{MathML} y al soporte del estándar \LaTeX. De esta forma puede ser compatible con la mayor cantidad de productos del mercado.
	
	\medskip
	
	Además, ha invertido recursos en la creación de distintas formas de creación de fórmulas, ya sea de forma manuscrita o con ayuda de una barra de herramientas. Por lo que la comunidad ha agradecido este avance.
	
	\medskip
	
	Finalmente, ha creado un ecosistema de aplicaciones de escritorio y web donde se utilizan imágenes como elementos genéricos para exportar sus fórmulas.
	
	\section{Comparativa de productos similares}
	
	Por lo tanto, \textit{MathType} no es el único software que permite crear y editar fórmulas matemáticas, pero sí que es uno de los más completos y hay motivos que justifican la creación de un software que extienda de él.
	
	\medskip
	
	Primero de todo tenemos el ecosistema de \textit{MathType}. \textit{MathType} ofrece ya dos versiones de su editor: una versión desktop y una versión web, por lo cual crear una versión para la plataforma iOS amplía el ecosistema ya existente.
	
	\medskip
	
	El segundo motivo es que no existen aplicaciones en iOS que permitan crear y editar fórmulas manuscritas.
	
	\medskip
	
	El tercer motivo es que no existen aplicaciones pensadas para crear y editar fórmulas y exportar a otras aplicaciones la imagen resultante con facilidad. Esto limita al usuario si quiere utilizar un editor en particular y complementarlo con un software externo.
	
	\medskip
	
	El cuarto motivo es que en general los editores de ecuaciones existentes en iOS no son cómodos y/o no aprovechan características del sistema y del dispositivo. No están pensados para dividir la pantalla en dos y utilizar dos aplicaciones a la vez. Por otra parte las funcionalidades táctiles que podrían ser útiles, como insertar fórmulas manuscritas, son inexistentes.
	
	\medskip
	
	De los motivos explicados se han extraído características que se creen relevantes para los usuarios y se ha realizado una tabla comparativa entre los diferentes programas existentes en el mercado. Además, se muestran las características que debería cumplir \textit{MathType for iOS} una vez terminado el proyecto.\cite{MathTypeWebReference}\cite{MicrosoftWordIOS}\cite{MathMagic}\cite{MathPad}\cite{IWork}
	
	\begin{adjustbox}{center,caption={Tabla comparativa de productos},nofloat=figure}
		\label{Tabla_Comparativa}
		% This is a non-float method. Look more here https://tex.stackexchange.com/questions/8625/force-figure-placement-in-text.
		\includegraphics[width=\textwidth,height=\textheight,keepaspectratio]{Tabla_ComparativaProductosDelMercado.png}
	\end{adjustbox}

	\textit{Microsoft Word} y \textit{iWork} pese a no ser programas pensados desde un inicio para la inserción de fórmulas matemáticas han ganado buena fama extendiendo sus funcionalidades. Sin embargo, ofrecen herramientas muy simples o a veces incómodas de usar cuando se quiere introducir una fórmula compleja. Todo esto debido a que tienes que navegar por demasiados menús. Pese a ello, se sigue trabajando y en las últimas versiones de \textit{Microsoft Word}\cite{MicrosoftWordFormula2Latex} se están realizando avances para soportar estándares como \LaTeX.
	
	\medskip
	
	En cuanto a \textit{MathPad}, es un editor de documentos centrado en poder escribir texto con la capacidad de insertar también fórmulas. Funciona realmente bien para crear fórmulas complejas pero no aprovecha las capacidades del sistema como es la multiventana. Además está muy limitado debido a que está pensado para trabajar en él y no para ser el complemento de otros programas, por lo que no cuenta con el soporte de formatos como \textit{MathML} o \LaTeX.
	
	\medskip
	
	Finalmente, \textit{MathMagic} es el programa más parecido a \textit{MathType} en su versión de escritorio. Debido a esto tiene grandes contras como el de no tener soporte para plataformas móviles y estar restringido a ser utilizado como complemento de programas específicos como \textit{Microsoft Word} o \textit{Adobe InDesign} y no como identidad propia o complemento genérico.
	
	\begingroup
	\renewcommand{\cleardoublepage}{}
	\renewcommand{\clearpage}{}
	\chapter{Formulación del problema}
	\endgroup
	
	\section{Problema}
	
	Actualmente hay muchos usuarios en iOS que editan documentos desde su iPad -y según las estadísticas de uso internas de \textit{MathType} seguirá creciendo. Sin embargo, cuando necesitan insertar fórmulas matemáticas es difícil crearlas y editarlas debido a que no hay editores matemáticos para el programa específico o tienes que usar editores alternativos.
	
	\medskip
	
	En cuanto a cambiar de editor, hay muchas empresas que trabajan con herramientas como \textit{Microsoft Word}\cite{MicrosoftWordIOS} y no están dispuestas a cambiar de software solamente porque en uno se pueda o sea más fácil insertar fórmulas matemáticas. Entonces acaban por recurrir a tener documentos en distintos editores, separados entre sí o sencillamente no insertar todo lo que les gustaría insertar.
	
	\medskip
	
	Además, es habitual que los editores de fórmulas matemáticas presenten interfaces incómodas que no se adaptan a la pantalla táctil de estos dispositivos. Tampoco existe la posibilidad de trabajar en modo multiventana (también se le puede llamar \enquote*{ventana partida}) debido a las mismas carencias en la interfaz de usuario.
	
	\section{Objetivos}
	
	Debido a esto, este proyecto busca crear una aplicación para el sistema operativo iOS. Esta aplicación debe permitir la creación y edición de fórmulas, que permita una vez creadas poder exportar la imagen a otra aplicación de edición de texto, como por ejemplo \textit{Microsoft Office}.
	
	\medskip
	
	Además, debe tener una interfaz adaptada a las características que ofrece tanto el sistema operativo iOS como las capacidades del dispositivo. Es decir, hacer uso de la capacidad multiventana y tener una interfaz usable teniendo en cuenta que es un dispositivo táctil y con dimensiones más reducidas que un monitor de ordenador.
	
	\begingroup
	\renewcommand{\cleardoublepage}{}
	\renewcommand{\clearpage}{}
	\chapter{Alcance}
	\endgroup
	
	Este proyecto busca extender el ecosistema de \textit{MathType} al sistema operativo iOS, para su uso principal en iPad, pero dando soporte también a iPhone. De esta manera, los usuarios contarán con una herramienta para poder trabajar en la edición de fórmulas matemáticas desde dispositivos iOS.
	
	\medskip
	
	El resultado del proyecto debe ser una aplicación iOS que contenga todas las funcionalidades de \textit{MathType} Web, compartiendo su código por temas de mantenimiento y aplicándole los elementos necesarios para extender sus funcionalidades y así ser más cómodo y útil en iOS.
	
	\medskip
	
	Los elementos que extenderán dichas funcionalidades de \textit{MathType} son elementos de pantalla para mejorar el copia y pega de las fórmulas, ajustes de imagen y herramientas, una sección de \enquote*{fórmulas recientes}, la posibilidad de exportar e importar imágenes de las fórmulas generadas mediante el uso de Drag\&Drop y por último, extender el estilo del editor para poder ser utilizado en pantalla dividida junto a otra aplicación. Esta última característica, solamente podrá ser utilizada en dispositivos iPad debido a que son los únicos dispositivos con la funcionalidad de multiventana.
	
	\section{Posibles riesgos y soluciones}
	
	En el transcurso del desarrollo del proyecto pueden surgir diferentes problemas. Los más relevantes son los siguientes:
	
	\begin{enumerate}
		\item \textbf{Tiempo insuficiente.} El tiempo del que se dispone para la realización es limitado. Durante la planificación del proyecto se tendrá en cuenta de manera constante. Una de las soluciones es hacer un sistema modular donde una vez realizada la integración de \textit{MathType Web} en iOS cada funcionalidad sería un módulo independiente. En caso de no tener el tiempo suficiente, podría no llegar a introducirse y todo el proyecto seguiría en funcionamiento.
		\item \textbf{Problemas al conectar diferentes lenguajes de programación entre sí.} Este proyecto hace uso de diferentes lenguajes de programación que pueden llegar a realizar tareas complejas, pero se podrían ver limitados en la comunicación entre sí. Una solución es desacoplar los diferentes módulos de la aplicación para que así la comunicación sea lo más simple posible. Cada módulo deberá trabajar de forma independiente y comunicar únicamente los resultados.
		\item \textbf{Ignorancia en la temática de diseño.} Durante la carrera no hay una gran enseñanza del diseño de interfaces y por lo tanto podría provocar que la aplicación no fuera tan fácil de usar como se quiere o que se gaste demasiado tiempo en ello. Una solución es aprovechar los recursos humanos de la empresa \textit{Maths for More S.L.} , ya que existe un departamento específico de documentación y diseño.
		\item \textbf{Aprendizaje de nuevas tecnologías.} No se tiene una gran experiencia en el lenguaje de programación. En caso de tener problemas, existen muchos recursos en Internet y habrá que invertir tiempo del proyecto a investigar. Se tendrá entonces en cuenta este tiempo extra durante la planificación temporal del proyecto. 
	\end{enumerate}
	
	\begingroup
	\renewcommand{\cleardoublepage}{}
	\renewcommand{\clearpage}{}
	\chapter{Metodología y rigor}
	\endgroup
	
	Durante la carrera se han explicado diferentes metodologías para afrontar proyectos, entre las cuales las más destacadas han sido \textit{Waterfall}\cite{SoftwareEngineeringBook_Waterfall} y \textit{Agile}\cite{SoftwareEngineeringBook_AgileChapter}.
	
	\medskip
	
	Como se nos ha explicado en asignaturas como GPS (Gestión de Proyectos Software), para saber cuál escoger hace falta observar la naturaleza del proyecto. Éste es un proyecto con los requisitos bien definidos pero que puede presentar muchas variaciones en el tiempo de ejecución debido al uso de tecnologías nuevas para el desarrollador y también el cambio de interfaz a una interfaz móvil.
	
	\medskip
	
	Además y debido a la limitación de recursos al haber un sólo desarrollador, se necesita tener el sistema funcionando lo antes posible para recibir \textit{feedback} y cambiar las cosas que sean necesarias.
	
	\medskip
	
	Por estos motivos, y siguiendo los principios del \textit{Manifiesto Agile}\cite{AgileManifesto} de dar más valor al software funcionando respecto a la documentación exhaustiva y la respuesta ante el cambio sobre seguir el plan, se ha decidido escoger \textit{Agile} como la metodología para este proyecto.
	
	\section{Método de trabajo}
	
	Una vez se ha decidido la metodología de trabajo hace falta especificar qué método se acabará utilizando.
	
	\medskip
	
	Existen diferentes implementaciones de la metodología Agile, pero en este proyecto se utilizará \textit{SCRUM}\cite{TheScrummGuide_DefinicionScrumm} ya que es el que utiliza \textit{MathType} y se cree conveniente que el proyecto se sincronice desde un principio con la línea general de productos de la empresa.
	
	\medskip
	
	\textit{SCRUM} lo que propone a grandes rasgos es definir bloques temporales llamados \textit{sprints} o iteraciones donde al final de cada bloque hay una entrega parcial. Una entrega parcial sería una mejora del producto que ayuda a estar más cerca del objetivo del proyecto, pero que no tiene por qué conseguir que se cumpla el objetivo en sólo esa entrega.
	
	\medskip
	
	\textit{SCRUM}\cite{TheScrummGuide_FasesScrumm} permite definir sprints máximos de 4 semanas, que es el que utiliza MathType y por lo tanto el que usará este proyecto. Dentro de	cada sprint se puede dividir principalmente en las siguientes fases:
	
	\begin{itemize}
		\item \textbf{Planificación del sprint.} Se realiza al principio de cada sprint.
		\begin{itemize}
			\item Selección de los requisitos a cumplir en el sprint.
			\item Planificación de las tareas a realizar para alcanzar los requisitos propuestos.
		\end{itemize}
		\item \textbf{Daily Meetings.} Reuniones que se hacen idealmente cada día, pero que en el caso de este proyecto se hará de forma semanal mediante email para mantener informado tanto al ponente como al director del proyecto. Y, debido al carácter semanal, durante el transcurso del proyecto se llamarán a este tipo de reuniones \textit{Weekly Meetings}.
		\item \textbf{Refinamiento.} Durante todo el sprint se realiza un análisis del tiempo y esfuerzo dedicado a cada una de las tareas y se vuelven a aclarar los requisitos si hiciera falta.
		\item \textbf{Revisión del sprint.} Reunión al final del sprint donde se muestra a toda la empresa el desarrollo que se ha llevado a cabo durante la iteración y a poder ser va acompañado de una demostración en vivo.
		\item \textbf{Retrospectiva.} Se realiza al final del sprint y se analiza qué se ha hecho mal, qué se ha hecho bien y qué inconvenientes se han encontrado que no han permitido avanzar como se había planificado en un inicio.
	\end{itemize}
	
	\section{Herramientas de seguimiento}
	
	Existen muchas herramientas de seguimiento para un proyecto, para este en específico se han escogido varias dependiendo del contexto.
	
	\medskip
	
	A nivel de código se realizará un seguimiento utilizando \textit{Git}\cite{GitWiki}, una tecnología que permite separar el código en las llamadas \enquote*{ramas} para así mantener los desarrollos nuevos independientes de la parte estable del sistema. Además permite llevar un control de los cambios del código.
	
	\medskip
	
	A nivel de planificación de tareas se hará uso de \textit{Trello}\cite{TrelloWeb}, una plataforma web que permite la creación de tareas y llevar un control del estado de las mismas.
	
	\medskip
	
	Finalmente, a nivel temporal se utilizará la herramienta \textit{Microsoft Project}\cite{MicrosoftProjectWiki}, que permite llevar un seguimiento de las tareas en las diferentes fases del proyecto junto a los recursos utilizados. También permite la creación de artefactos como pueden ser los \textit{diagramas de Gantt}.
	
	\section{Método de validación}
	
	\textit{SCRUM} plantea el uso de tests como método de validación del código generado\cite{TheScrummGuide_ScrummArtifacts}\cite{TheScrummGuide_ScrummArtifactsTransparency}. Cada incremento del código que se genera por cada tarea tiene su conjunto de tests asociado. Estos tests tendrán que pasar correctamente antes de la entrega final. De esta manera es posible validar que los requisitos se cumplen y que no se ha dado el caso de poder haber hecho que otro requisito que antes se cumplía ahora no se cumpliera.
	
	\medskip
	
	Para el caso de las validaciones que no puedan llevarse a cabo con tests, como por ejemplo \enquote*{que la interfaz sea bonita}, se realizará una validación durante las reuniones finales de revisión del sprint y retrospectiva.
	
	\medskip
	
	Y finalmente, se informará de manera semanal en los \textit{Weekly Meetings} al director y al ponente del proyecto TFG para que opinen sobre el estado del proyecto. No obstante, también se realizarán reuniones espontáneas en caso de tener dudas que necesiten ser resueltas lo antes posible.
	
	\printbibliography
\end{document}	