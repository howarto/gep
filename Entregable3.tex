\documentclass[11pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage[hidelinks=true]{hyperref}
\usepackage[spanish,es-tabla]{babel}
\usepackage[backend=biber]{biblatex}
\usepackage{csquotes}
\usepackage{glossaries}
\usepackage{adjustbox}
\usepackage{float}
\usepackage{enumitem}
\usepackage[toc,page]{appendix}
\usepackage[ddmmyyyy]{datetime}
\usepackage{tabularx,ragged2e}
\newcolumntype{C}{>{\Centering\arraybackslash}X} % centered "X" column
\usepackage{caption}

\author{
	Diego Lao Tebar \\
	Ponente: Cristina Gómez Seoane \\
	Director: Juan Lao Tebar \\
	Profesor de GEP: Joan Carles Gil Martin \\
	Especialidad: Ingeniería del Software
}
\title{
	{Integración de un editor de expresiones matemáticas en un entorno educativo basado en dispositivos móviles} \\
	{\large GEP - Tercer entregable} \\
	{\large UPC - FIB} \\
	\vspace{1cm}
	{\includegraphics[width=0.5\columnwidth]{upc.png}}
}

\addbibresource{references.bib}
\graphicspath{{images/}}
% Propiedades del estilo del documento
\setlength\parindent{0pt}

\begin{document}
	\maketitle
	
	\pagebreak
	
	\tableofcontents
	
	\pagebreak
	
	\section{Gestión económica y sostenibilidad}
	
	Una vez realizada la planificación temporal existen datos suficientes como para desarrollar el presupuesto del proyecto. Éste justifica la viabilidad económica de cara a la empresa \textit{Maths for More S.L.} .
	
	\subsection{Identificación y estimación de costes}
	
	La identificación de costes es el paso previo a la elaboración del presupuesto. Esta sección analiza desde el punto de vista económico los diferentes tipos de recursos del proyecto, definidos previamente durante la planificación.
	
	\subsubsection{Costes directos}
	
	\paragraph{Recursos humanos.}
	
	Este proyecto está realizado íntegramente por el estudiante del TFG, asumiendo cada uno de los roles correspondientes durante la realización de cada tarea.
	
	\smallskip
	
	El coste por hora de cada rol ha sido extraído de la \textit{Guía del mercado laboral 2019}\cite{GuiaHays2019} de la empresa \textit{Hays}, una de las empresas de recursos humanos más grandes del mundo, y webs reconocidas como \textit{PayScale}\cite{TechnicalWriterSalaryBarcelona} e \textit{Indeed}\cite{TraductorSalaryBarcelona}. Los precios se han calculado en base a un trabajo anual de 2008h y un nivel de experiencia equivalente a \enquote*{junior}, es decir, entre 0 y 2 años de experiencia.
	
	\smallskip
	
	De esta investigación, se ha obtenido la siguiente tabla con los costes por tarea, fase y rol.

	\vspace{5mm}
	
	\begin{minipage}{\linewidth}
		\captionof{table}{Coste de RRHH por rol, fases y tareas}
		\begin{adjustbox}{center,nofloat=figure}
			
			% This is a non-float method. Look more here https://tex.stackexchange.com/questions/8625/force-figure-placement-in-text.
			\includegraphics[width=\textwidth,keepaspectratio]{Tabla_Coste_RRHH1.png}
		\end{adjustbox}
	\end{minipage}
	
	\begin{minipage}{\linewidth}
		\begin{adjustbox}{center,nofloat=figure}
			% This is a non-float method. Look more here https://tex.stackexchange.com/questions/8625/force-figure-placement-in-text.
			\includegraphics[width=\textwidth,keepaspectratio]{Tabla_Coste_RRHH2.png}
		\end{adjustbox}
	\end{minipage}

	\paragraph{Recursos hardware y software.} Por otro lado, también existe un gasto en los recursos hardware y software desde el inicio del proyecto.
	
	\smallskip
	
	Aquí se muestran solamente aquellos recursos que han supuesto un coste, ya que gran parte del software utilizado es gratuito.
	
	\begin{minipage}{\linewidth}
		\begin{adjustbox}{center,nofloat=figure}
			
			% This is a non-float method. Look more here https://tex.stackexchange.com/questions/8625/force-figure-placement-in-text.
			\includegraphics[width=0.5\textwidth,keepaspectratio]{Tabla_Coste_Recursos.png}
		\end{adjustbox}
		\captionof{table}{Coste de recursos materiales y software}
	\end{minipage}
	
	\subsubsection{Costes indirectos}
	
	Durante el desarrollo existen costes indirectos como consecuencia de las actividades que se realizan. Dado que el TFG se realiza en una empresa, es el coste de recursos generales (agua, oficina, Internet... etc).
	
	\smallskip
	
	Los costes indirectos por lo tanto son los siguientes:
	
	\begin{minipage}{\linewidth}
		\begin{adjustbox}{center,nofloat=figure}
			% This is a non-float method. Look more here https://tex.stackexchange.com/questions/8625/force-figure-placement-in-text.
			\includegraphics[width=0.45\textwidth,keepaspectratio]{Tabla_Coste_Indirecto.png}
		\end{adjustbox}
		\captionof{table}{Costes indirectos}
	\end{minipage}
	
	\subsubsection{Contingencias}
	
	Como todo proyecto, existe un riesgo de necesitar un presupuesto mayor al elaborado debido a los sucesos inesperados. Por ello existe una partida destinada a contener esas posibles desviaciones. Para este proyecto se utilizará un fondo de contingencias del 10\%, una cifra encontrada de manera empírica y que en asignaturas como GPS se nos ha impulsado a utilizar por su buen resultado.
	
	\subsubsection{Coste de incidentes}
	
	Finalmente se presenta el coste de los incidentes que pueden surgir en el proyecto.
	
	\smallskip
	
	Para ello, se tiene en cuenta el caso pesimista de los problemas presentados en el plan de acción de la planificación, donde se habla del incremento de horas de trabajo. Este aumento de horas probablemente se daría a la mitad del proyecto, durante la fase de \textit{Desarrollo de MathType for iOS}. Debido a eso se ha calculado un coste asumiendo que el aumento de horas empieza en la tarea \enquote*{Cambio en el tipo de imagen} hasta la finalización del proyecto.
	
	\smallskip
	
	Por otra parte, se ha tenido en cuenta la pérdida del Mac Mini, la herramienta de desarrollo principal. Cuyo coste, gracias a que la entrega podría ser el mismo día y que el estudiante tiene horario flexible, se limitaría a un coste material.
	
	\begin{minipage}{\linewidth}
		\begin{adjustbox}{center,nofloat=figure}
			
			% This is a non-float method. Look more here https://tex.stackexchange.com/questions/8625/force-figure-placement-in-text.
			\includegraphics[width=0.5\textwidth,keepaspectratio]{Tabla_Coste_Incidentes.png}
		\end{adjustbox}
		\captionof{table}{Costes de incidentes}
	\end{minipage}
	
	\subsection{Coste total}
	
	De las secciones anteriores, se puede extraer la siguiente tabla con el presupuesto del proyecto.
	
	\begin{minipage}{\linewidth}
		\begin{adjustbox}{center,nofloat=figure}
			
			% This is a non-float method. Look more here https://tex.stackexchange.com/questions/8625/force-figure-placement-in-text.
			\includegraphics[width=0.35\textwidth,keepaspectratio]{Tabla_Coste_Total.png}
		\end{adjustbox}
		\captionof{table}{Coste total}
	\end{minipage}
	
	\subsection{Control de gestión}
	
	Como método de control económico del proyecto, al finalizar cada tarea se anotarán las horas y recursos invertidos reales. Dichos datos se compararán con las estimaciones realizadas haciendo uso de las siguientes fórmulas presentadas durante la asignatura de GEP\cite{ApuntesAsignaturaGEP}.
	
	\begin{equation}
		\begin{split}
			Desv\acute{i}oEnCoste &= (CE - CR) \cdot CHR \\
			Desv\acute{i}oEnConsumo &= (CHE - CHR) \cdot CE \\
		\end{split}
	\end{equation}
	
	\smallskip
	
	Donde: CE=Coste Estimado; CR=Coste Real; CHR=Consumo de Horas Real; CHE=Consumo de Horas Estimado.
	
	\smallskip
	
	Gracias a esto, es posible saber las desviaciones del trabajo y si dicha desviación puede ser soportada por el proyecto junto a la partida destinada a contingencias.

	\subsection{Sostenibilidad}
	
	Para identificar la sostenibilidad del proyecto, se evalúa el impacto de tres aspectos del mismo: económico, ambiental y social. A continuación se explica cada aspecto en el contexto del TFG.

	\subsubsection{Dimensión económica}
	
	Se ha realizado un análisis de costes preciso para el proyecto teniendo en cuenta todo tipo de costes: directos, indirectos, contingencias y costes debido a incidentes. Además, dicho análisis se ha realizado respecto a las fases y tareas, por lo que su seguimiento y control es más preciso al ser un intervalo de tiempo y conjunto de recursos más acotado. Por esto mismo, se considera que la estimación de costes ha sido razonable.
	
	\smallskip
	
	Por otra parte, este proyecto se ve respaldado con cifras internas de la empresa \textit{Maths for More S.L.} respecto a la demanda de un producto de \textit{MathType} para iOS que hará superar el presupuesto del proyecto. Sin embargo, dicha previsión se espera de aquí a 1-2 años. Debido a esta cantidad de tiempo, \textit{Maths for More S.L.} ha decidido destinar los recursos de una sola persona para su desarrollo.
	
	\smallskip
	
	Finalmente, otras soluciones alternativas tienen problemas por tener recursos limitados. Son pequeñas empresas, que se dedican solamente a la edición de fórmulas matemáticas; o grandes empresas, donde la edición de fórmulas matemáticas es solamente una funcionalidad más de su programa y a la que destinan una cantidad de recursos muy limitada. En ambos casos la estimación de recursos se realiza en función de los recursos humanos necesarios y orden de importancia.
	
	\smallskip
	
	A diferencia de estas, \textit{Maths for More S.L.} y su producto \textit{MathType} se dedica específicamente a la edición de fórmulas matemáticas y tiene los recursos suficientes para desarrollar varios proyectos al mismo tiempo, pudiendo reducir el coste material reutilizando gran parte de los recursos.

	\subsubsection{Dimensión ambiental}
	
	Durante el proyecto, se ha tenido en cuenta la reutilización de recursos. Un ejemplo ha sido el ordenador portátil o el iPad Pro, utilizados en diferentes proyectos. Sin embargo, no se ha podido hacer lo mismo para el caso del Mac Mini y la electricidad consumida.
	
	\smallskip
	
	Se prevé que la electricidad consumida por estos equipos\cite{MacMiniConsumoWebsite}\cite{DellConsumoWebsite}\cite{DellConsumoWebsite2}\cite{iPadProConsumoWebsite} sea de 37,5kW·h de media. Por otro lado,	no se tiene en cuenta los gastos generales.
	
	\smallskip
	
	Además, la solución proporcionada mejora entre otras cosas la rapidez respecto a otras soluciones similares. Esto provoca una reducción del consumo, ya que los usuarios utilizan menos tiempo su dispositivo al haber terminado antes el trabajo. Una reducción del tiempo de uso de los usuarios de un 15\% supone una disminución de energía consumida en el iPad Pro de 11,5kW·h a 9,775kW·h (Sin tener en cuenta pérdidas).
	
	\smallskip
	
	Las soluciones alternativas no tienen en cuenta la eficiencia energética y la interfaz que presentan, con colores vivos y más lentas de utilizar, gastan más energía.
	
	\begin{minipage}{\linewidth}
		\begin{adjustbox}{center,nofloat=figure}
			
			% This is a non-float method. Look more here https://tex.stackexchange.com/questions/8625/force-figure-placement-in-text.
			\includegraphics[width=0.5\textwidth,keepaspectratio]{Tabla_Energia_Consumida.png}
		\end{adjustbox}
		\captionof{table}{Energía eléctrica consumida}
	\end{minipage}
	
	\subsubsection{Dimensión social}
	
	No existe ningún editor específico de fórmulas matemáticas en iOS, pero según estadísticas internas existen usuarios de \textit{MathType} que les gustaría poder trabajar en dicha plataforma. Esta solución aporta en el ámbito social una mejora en el trabajo de las personas que necesiten insertar o editar fórmulas matemáticas. Las alternativas ofrecen una mala experiencia de usuario, en cambio ésta tiene una mejor interfaz, provocando que el proceso sea menos tedioso y más rápido. Por lo tanto serán más eficientes y disminuirán el tiempo dedicado a un trabajo poco gratificante.
	
	\smallskip
	
	Finalmente, este proyecto aporta experiencia al estudiante. Tiene la posibilidad de aplicar sus conocimientos en un entorno nuevo para él y gracias a que es un proyecto \textit{open source}, puede mostrarlo como ejemplo de su trabajo.
	
	\pagebreak
	\printbibliography
\end{document}	