\documentclass[11pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage[hidelinks=true]{hyperref}
\usepackage[spanish,es-tabla]{babel}
\usepackage[backend=biber]{biblatex}
\usepackage{csquotes}
\usepackage{glossaries}
\usepackage{adjustbox}
\usepackage{float}
\usepackage{enumitem}
\usepackage[toc,page]{appendix}
\usepackage[ddmmyyyy]{datetime}
\usepackage{tabularx,ragged2e}
\newcolumntype{C}{>{\Centering\arraybackslash}X} % centered "X" column
\usepackage{caption}

\author{
	Diego Lao Tebar \\
	Ponente: Cristina Gómez Seoane \\
	Director: Juan Lao Tebar \\
	Profesor de GEP: Joan Carles Gil Martin \\
	Especialidad: Ingeniería del Software
}
\title{
	{Integración de un editor de expresiones matemáticas en un entorno educativo basado en dispositivos móviles} \\
	{\large GEP - Segundo entregable} \\
	{\large UPC - FIB} \\
	\vspace{1cm}
	{\includegraphics[width=0.5\columnwidth]{upc.png}}
}

\addbibresource{references.bib}
\graphicspath{{images/}}
% Propiedades del estilo del documento
\setlength\parindent{0pt}

\begin{document}
	\maketitle
	
	\pagebreak
		
	\tableofcontents
	
	\pagebreak

	% Uncomment the next line when it will be inserted in TFG.
	% Also remove the section definition line.
	% \chapter{Planificación temporal}
	\section{Planificación temporal}
	
	La planificación de un proyecto es parte esencial de la gestión del mismo. Una buena planificación puede ser decisiva para lograr acabar dentro de los plazos necesarios y por lo tanto cumplir los objetivos planteados.
	
	\smallskip
	
	Este proyecto TFG tiene una duración de 540h, siguiendo así la normativa académica aprobada en 2015\cite{NormativaFibTfgWebsite}. De las cuales 75h son de la asignatura de GEP y las 465h restantes quedan para realizar el proyecto de forma totalmente autónoma. El inicio tiene fecha \formatdate{18}{01}{2019} y la fecha máxima es la de su defensa, en junio o julio del mismo año, aunque se ha estimado que para el \formatdate{05}{05}{2019} ya estará acabado.
	
	\smallskip
	
	La finalidad de este capítulo es la de explicar las diferentes fases que se esperan realizar y las tareas asociadas. Además de aproximar el gasto de recursos en cada tarea y definir la hoja de ruta a seguir.
	
	\section{Descripción de las fases y tareas asociadas}
	
	En esta sección se muestran las siete fases en las que queda dividido el proyecto, junto a sus tareas y recursos asignados. Cabe destacar que recursos como el espacio de trabajo (silla, mesa, oficina...) han sido obviados dado el análisis tan exhaustivo que precisa.
	
	\smallskip
	
	Para la estimación temporal, se ha utilizado la hipótesis de que se realizan 35h semanales como jornada de trabajo.
	
	\vspace{5mm}
	
	\begin{minipage}{\linewidth}
		\captionof{table}{Planificación temporal de fases, tareas y recursos}
		% The label needs to be AFTER the caption.	
		\label{table:Tabla_PlanificacionTareasTiemposYRecursos2}
		\begin{adjustbox}{center,nofloat=figure}

			% This is a non-float method. Look more here https://tex.stackexchange.com/questions/8625/force-figure-placement-in-text.
			\includegraphics[width=0.55\textwidth,keepaspectratio]{Tabla_PlanificacionTareasTiemposYRecursos1.png}
		\end{adjustbox}
	\end{minipage}
	
	\begin{minipage}{\linewidth}
		\begin{adjustbox}{center,nofloat=figure}
			% This is a non-float method. Look more here https://tex.stackexchange.com/questions/8625/force-figure-placement-in-text.
			\includegraphics[width=0.55\textwidth,keepaspectratio]{Tabla_PlanificacionTareasTiemposYRecursos2.png}
		\end{adjustbox}
	\end{minipage}
	
	\subsection{Fases y tareas}
	
	\subsubsection{Asignatura GEP}
	
	La siguiente fase consta de la elaboración de los entregables de la asignatura GEP, que ayudan a contextualizar, planificar y explicar el proyecto. Además se tiene en cuenta las sesiones presenciales y la presentación final que hay que realizar. El tiempo de esta fase se ha estimado según el global de horas de la asignatura (75h) y una estimación personal después de realizar el primer y segundo entregable. Hay que destacar que esta fase provoca una pausa en el proyecto empezado ya en enero.
	
	\subsubsection{Análisis}
	
	En esta fase del proyecto se realiza el análisis técnico de la aplicación desde el punto de vista de la ingeniería de software para su posterior implementación. Por ello, las principales tareas que se tienen que realizar son la toma de requisitos, el diseño y especificación de la arquitectura y el análisis del diseño de la interfaz.

	\subsubsection{Preparación del entorno de trabajo}
	
	Otra fase del proyecto es preparar el entorno de trabajo. Esta fase tiene en cuenta la instalación y configuración del entorno de trabajo necesario para empezar a desarrollar tanto en local como en los servidores de integración continua de la empresa \textit{Maths for More S.L.} . Las principales tareas por lo tanto son: la preparación del ordenador de desarrollo y la preparación de los servidores de integración continua.

	\subsubsection{Pruebas de concepto y automatización}
	
	Para el buen desarrollo del proyecto es necesario realizar diferentes pruebas de concepto de las cuales elegir las mejores para implementar en el proyecto final. Todo ello ocupa una fase del proyecto enfocada al aprendizaje, pruebas y automatización. Por lo tanto las tareas relacionadas son: el autoaprendizaje de las diferentes tecnologías a utilizar; las pruebas de concepto para así asentar los conocimientos adquiridos y ver cómo adaptar la tecnología a la especificación del proyecto; y la automatización del compilado y testeo de los proyectos de la tecnología utilizada.
	
	\subsubsection{Desarrollo de \textit{MathType for iOS}}

	Esta fase es la más grande de todas, es la parte principal del proyecto y la que genera el producto \textit{MathType for iOS} que la empresa precisa.
	
	\smallskip
	
	Primero de todo se encuentra la tarea de creación de la aplicación base para iOS. De ella se obtiene la integración del editor MathType Web en iOS.
	
	\smallskip
	
	A raíz de ello, se realizan una serie de tareas que extienden una a una las funcionalidades que se ofrecen, para así cumplir aquellos objetivos planteados en la formulación del problema.
	
	\smallskip
	
	Concretamente serían: el importar y exportar fórmulas mediante el uso de \textit{Drag\&Drop}, la reedición de fórmulas creadas, la visualización de fórmulas recientes, el cambio de tipo de imagen, el uso de la cámara para detectar fórmulas y el soporte a distintos idiomas.
	
	\smallskip
	
	Las tareas que extienden las funcionalidades de la aplicación son completamente independientes y pueden realizarse en paralelo o en el orden que se considere oportuno. El orden de realización de éstas es el que se puede observar en la Tabla \ref{table:Tabla_PlanificacionTareasTiemposYRecursos2}. Este orden ha sido escogido según las preferencias de la empresa.
	
	\subsubsection{Documentación}
	
	Una de las fases finales del proyecto es la documentación. Esta fase es la encargada de generar toda la \textit{documentación de usuario}, para que así las personas que utilizan \textit{MathType for iOS} puedan consultar cómo utilizar el programa en detalle.
	
	\subsubsection{Memoria y presentación}
	
	En esta fase se terminará la memoria del proyecto que se ha ido haciendo durante la asignatura de GEP y se revisará. Finalmente, se preparará la presentación de la defensa del proyecto.
	
	\smallskip
	
	La memoria del proyecto incluirá la documentación realizada a lo largo de la asignatura de GEP, la fase de análisis y la fase de documentación.
	
	\section{Diagrama de Gantt}
	
	En el diagrama de Gantt correspondiente a la planificación del proyecto se incluyen todas las fases y tareas especificadas anteriormente. Este diagrama muestra los tiempos y también los órdenes de realización de las diferentes tareas durante el proyecto. El diagrama es el siguiente:
	
	\begin{adjustbox}{center,caption={Diagrama de Gantt},nofloat=figure}
		% This is a non-float method. Look more here https://tex.stackexchange.com/questions/8625/force-figure-placement-in-text.
		\includegraphics[width=\textwidth,height=\textheight,keepaspectratio]{Diagrama_Gantt1.png}
	\end{adjustbox}

	\begin{adjustbox}{center,caption={Continuación diagrama de Gantt},nofloat=figure}
		% This is a non-float method. Look more here https://tex.stackexchange.com/questions/8625/force-figure-placement-in-text.
		\includegraphics[width=\textwidth,height=\textheight,keepaspectratio]{Diagrama_Gantt2.png}
	\end{adjustbox}

	\section{Plan de acción}

	Como se comentó en otros capítulos, los riesgos de este proyecto se deben a la tecnología. Se hace uso de diferentes tecnologías donde el integrador del proyecto no es experto. Además existen tareas como la de documentación o diseño de interfaces que pueden suponer un costo de tiempo más elevado al salirse del ámbito de trabajo.
	
	\medskip

	El plan de acción de este proyecto está basado principalmente en el control de situaciones de riesgo. Por ello, durante la realización del proyecto hay un seguimiento constante del trabajo realizado y del tiempo restante.
	
	\medskip
	
	Para dicho seguimiento se han establecido dos protocolos de acción en caso de detectar una falta de tiempo.
	
	\medskip
	
	Por una parte, todas las tareas han sido sobreestimadas en un 15\% para tener margen de maniobra en caso de haber estimado el tiempo por debajo de la realidad.
	
	\medskip
	
	Por otra parte, está planteada también la posibilidad de aumentar el número de horas dedicadas al proyecto. Actualmente se realizan 5h diarias, podría llegar a subirse hasta en 7h diarias.
	
	\medskip
	
	En caso de que lo anterior fallara, se tomaría como medida de urgencia eliminar tareas de la fase de \textit{Desarrollo de MathType for iOS}. Ya que el soporte de idiomas, el elegir el tipo de imagen y también la sección de \enquote*{fórmulas recientes} no son necesarios para el correcto funcionamiento de la aplicación. Por lo tanto, se puede prescindir de ellas y que la aplicación siga siendo útil y funcional para el usuario.
	
	\medskip
	
	Estas decisiones propuestas permitirían la entrega del proyecto en el plazo establecido. Sin embargo, el costo de recursos humanos se incrementaría un 40\% por hora desde el momento en el que se aumentaran las horas diarias. Y por otro lado, la aplicación de la medida de urgencia, pese a funcionar, disminuiría la calidad del producto final.
	
	\pagebreak
	\printbibliography
\end{document}	